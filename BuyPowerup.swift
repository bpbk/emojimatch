//
//  BuyPowerup.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 11/12/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit
import StoreKit

class BuyPowerup: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDescription: UITextView!
    @IBOutlet weak var buyButton: UIButton!
    
    var selectedProductIndex: Int!
    var transactionInProgress = false
    var productRequest = SKProductsRequest()
    
    var product: SKProduct?
    var productIDs: Array<String!> = []
    var productsArray: Array<SKProduct!> = []
    
    override init() {
        super.init()
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        productIDs.append("extralife1")
        productRequest.delegate = self
        requestProductInfo()
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print(transactions)
    }
    func requestProductInfo() {
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = NSSet(array: productIDs)
            print(productIdentifiers)
            productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        //print("hey: \(request)")
        print(response)
        print("fuck you")
        if response.products.count != 0 {
            for product in response.products {
                productsArray.append(product)
                print(product)
            }
        }
        else {
            print("There are no products.")
        }
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    /*func showActions() {
    if transactionInProgress {
    return
    }
    
    let actionSheetController = UIAlertController(title: "IAPDemo", message: "What do you want to do?", preferredStyle: UIAlertControllerStyle.ActionSheet)
    
    let buyAction = UIAlertAction(title: "Buy", style: UIAlertActionStyle.Default) { (action) -> Void in
    
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in
    
    }
    
    actionSheetController.addAction(buyAction)
    actionSheetController.addAction(cancelAction)
    
    //presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
    switch transaction.transactionState {
    case SKPaymentTransactionState.Purchased:
    print("Transaction completed successfully.")
    SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    transactionInProgress = false
    delegate.didBuyColorsCollection(selectedProductIndex)
    
    
    case SKPaymentTransactionState.Failed:
    print("Transaction Failed");
    SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    transactionInProgress = false
    
    default:
    print(transaction.transactionState.rawValue)
    }
    }
    }
    
    func didBuyColorsCollection(collectionIndex: Int) { // CALLING A FUNCTION INSIDE THE OBJECT USING THE DELEGATE
    if collectionIndex == 0 {
    
    }
    else {
    
    }
    }*/
}