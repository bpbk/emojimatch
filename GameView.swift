//
//  GameView.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/22/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class GameView: UIViewController {

    private var buttonArray = [MatchButton]()
    private var currentSelection = 0
    private var doubledArray: [NSArray] = []
    private var selectedArray: [Int] = []
    private var attemptsMade = 0
    var matchArray:Array<String>?
    private var matchesInGame:Int?
    private var matchesMatched = 0
    private let matchCount = 2
    private var arrayOfAttempts:Array<Int> = []
    var gameInfo:GameObject!
    
    @IBOutlet var alertText: UILabel?
    @IBOutlet var movesLeft: UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        matchArray = gameInfo.tileArray
        matchesInGame = matchArray?.count
        gamestats.currentlives += gameInfo.movesInGame
        updateMoves() // UPDATE MOVES LEFT IN GAME
        var matchCounter = 1
        for (i, match) in matchArray!.enumerate(){
            for _ in 1...matchCount{
                let matchWithIndex = [match, matchCounter, i + 1]
                matchCounter++
                print(matchCounter)
                doubledArray.append(matchWithIndex)
            }
        }
        let shuffledArray = doubledArray.shuffle
        for (i , match) in shuffledArray.enumerate(){
            let matchBtn = MatchButton(frame: CGRectMake((100 * CGFloat(i+1)) + (CGFloat(i+1) * 20), 50, 100, 100), bgColor: UIColor.clearColor(), initImage: match[0] as! String, initIndex: match[1] as! Int, matchId: match[2] as! Int)
            
            matchBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            matchBtn.canBecomeFocused()
            matchBtn.addTarget(self, action: Selector("buttonTouched:"), forControlEvents: .PrimaryActionTriggered)
            
            self.view!.addSubview(matchBtn)
            print(matchBtn.imageName)
            buttonArray.append(matchBtn)
        }
        // Do any additional setup after loading the view.
    }

    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        context.nextFocusedView!.layer.shadowOffset = CGSizeMake(0, 10);
        context.nextFocusedView!.layer.shadowOpacity = 0.6;
        context.nextFocusedView!.layer.shadowRadius = 15;
        context.nextFocusedView!.layer.shadowColor = UIColor.blackColor().CGColor;
        if (context.previouslyFocusedView != nil){
            context.previouslyFocusedView!.layer.shadowOpacity = 0;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buttonTouched(sender: MatchButton) {
        UIView.transitionFromView(sender.matchButtonView, toView: sender.matchImageView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
        sender.userInteractionEnabled = false
        preferredFocusedView?.viewWithTag(currentSelection+1)
        selectedArray.append(sender.matchIndex)
        print(selectedArray)
        if (currentSelection == 0){ // FIRST TILE WAS FLIPPED
            currentSelection = sender.matchId
        }else{ // SECOND TILE WAS FLIPPED
            if (sender.matchId == currentSelection){ // 2 SIMILAR TILES SELECTED
                var luckyMove = true // LUCKY STARTS OFF AS TRUE
                for btn in buttonArray{
                    if selectedArray.contains(btn.matchIndex){
                        if btn.attempted{ // IF ONE OF THE BUTTONS HAS BEEN ATTEMPTED, LUCKY IS NOW FALSE
                            luckyMove = false
                        }
                        btn.userInteractionEnabled = false;
                    }
                }
                if luckyMove{ // IF LUCKYMOVE IS STILL TRUE, IT'S A LUCKY MOVE
                    showAlert("LUCKYYYYY")
                }
                selectedArray = []
                currentSelection = 0
                matchesMatched++
            }else{ // WRONG TILES SELECTED
                NSTimer.scheduledTimerWithTimeInterval(0.7, target: self, selector: Selector("flipWrongTiles:"), userInfo: nil, repeats: false)
                
            }
            attemptsMade++
            gamestats.currentlives--
            updateMoves()
            if matchesMatched == matchesInGame{
                youWin()
            }
            if gamestats.currentlives == 0 && matchesMatched < matchesInGame || ((matchesInGame)! - matchesMatched > gamestats.currentlives){
                youLose()
            }
        }
    }
    
    func showAlert(alertMessage : String){
        alertText?.text = alertMessage
        alertText?.hidden = false
        NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("hideAlert:"), userInfo: nil, repeats: false)
    }
    
    func updateMoves(){
        movesLeft?.text = "\(String(gamestats.currentlives)) MOVES LEFT"
    }
    
    func hideAlert(timer : NSTimer){
        alertText?.hidden = true
    }
    
    func flipWrongTiles(timer : NSTimer) {
        for btn in buttonArray{
            print(btn.matchIndex)
            if selectedArray.contains(btn.matchIndex){
                
                btn.attempted = true
                UIView.transitionFromView(btn.matchImageView, toView: btn.matchButtonView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
                btn.userInteractionEnabled = true;
            }
        }
        selectedArray = []
        currentSelection = 0
    }
    
    func youWin(){
        showAlert("YOU DID IT IN \(attemptsMade) MOVES")
        NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("goBackHome:"), userInfo: nil, repeats: false)
    }
    
    func youLose(){
        showAlert("YOU LOSE")
        gamestats.currentlives = 0
        for btn in buttonArray{
                NSTimer.scheduledTimerWithTimeInterval(0.7, target: self, selector: Selector("flipWrongTiles:"), userInfo: nil, repeats: false)
            btn.userInteractionEnabled = true;
        }
        NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("goBackHome:"), userInfo: nil, repeats: false)
    }
    
    func goBackHome(timer : NSTimer){
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func pressesBegan(presses: Set<UIPress>, withEvent event: UIPressesEvent?) {
        if(presses.first?.type == UIPressType.Menu) {
            gamestats.currentlives = 0
        } else {
            // perform default action (in your case, exit)
            super.pressesBegan(presses, withEvent: event)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
