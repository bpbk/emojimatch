//
//  MapView.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 11/4/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class MapView: UIViewController {

    @IBOutlet var mapHolder: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupMap(){
        if let path = NSBundle.mainBundle().pathForResource("MapData", ofType: "plist") { // IF THERE'S A PLIST FILE
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> { // IF IT CAN MAKE A DICTIONARY FROM IT
                if let worldArray = dict["world_1"] as? Array<Dictionary<String, AnyObject>> { // IF THERE IS AN ENTRY FOR THIS WORLD
                    for (i , level) in worldArray.enumerate(){
                        let levelPos = level["position"] as? Array<Int>
                        print(levelPos![0])
                        let levelBtn = MapLevel(frame: CGRectMake(CGFloat(levelPos![0]), CGFloat(levelPos![1]), 50, 50), levelIndex: i)
                        self.mapHolder!.addSubview(levelBtn)
                    }
                }
            }
        }
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView != nil{
            context.nextFocusedView!.layer.shadowOffset = CGSizeMake(7, 7);
            context.nextFocusedView!.layer.shadowOpacity = 0.6;
            context.nextFocusedView!.layer.shadowRadius = 7;
            context.nextFocusedView!.layer.shadowColor = UIColor.blackColor().CGColor;
        }
        if (context.previouslyFocusedView != nil){
            context.previouslyFocusedView!.layer.shadowOpacity = 0;
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
