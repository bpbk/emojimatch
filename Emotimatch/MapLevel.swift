//
//  MapLevel.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 11/4/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class MapLevel: UIButton {
    
    let levelButtonView = UIView()
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    init(frame lFrame: CGRect, levelIndex lIndex: Int) {
        
        super.init(frame: lFrame)
        
        //addTarget(self, action: Selector("buttonTouched:"), forControlEvents: .PrimaryActionTriggered)
        backgroundColor = UIColor.clearColor();
        self.canBecomeFocused()
        self.userInteractionEnabled = true
        self.tag = lIndex
        //self.setTitle(mName, forState: .Normal)
        
        levelButtonView.backgroundColor = UIColor.greenColor()
        levelButtonView.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
        levelButtonView.makeCircle()
        
        self.addSubview(levelButtonView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
