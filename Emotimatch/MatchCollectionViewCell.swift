//
//  MatchCollectionViewCell.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/6/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class MatchCollectionViewCell: UICollectionViewCell {
    @IBOutlet var matchView: UIView!
    @IBOutlet var gameName: UILabel!
    @IBOutlet var gameTileCount: UILabel!

}

