//
//  SurvivalObject.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/28/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class SurvivalObject: NSObject {
    var errorsallowed = 0
    var currentlevel = 0
    var tilesinlevel = 1
    var flipTime = 0.3
    var gameTime = 0
    var currentTime = 0
    var gamePoints = 0
    var levelPoints = 0
    var streakCount = 0
    var luckiesCount = 0
    var tileColor: UIColor?
    var bgcolor_1: UIColor?
    var bgcolor_2: UIColor?
    var errorsForLevel: Int?
    
    let pointsForSpeed = 100
    let pointsForMatch = 10
    let pointsForErrors = 100
    let pointsForLucky = 500
    
    override init() {
        super.init()
    }
    
    func updateSurvivalStats(){
        self.errorsForLevel = Int(Float(Float(tilesinlevel) * 2 / 4).roundToInt() + 1)
        self.errorsallowed += errorsForLevel!
        self.currentlevel++
        self.tilesinlevel++
        self.flipTime += 0.2
        self.gameTime = 5 * tilesinlevel
        self.currentTime = gameTime
        self.streakCount = 0
        self.luckiesCount = 0
        self.levelPoints = 0
        if let path = NSBundle.mainBundle().pathForResource("SurvivalData", ofType: "plist") { // IF THERE'S A PLIST FILE
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> { // IF IT CAN MAKE A DICTIONARY FROM IT
                if let levelDictionary = dict["level_\(currentlevel)"] as? Dictionary<String, AnyObject> { // IF THERE IS AN ENTRY FOR THIS LEVEL
                    if let tileColorArray = levelDictionary["tilecolor"] as? Array<Int>{ // IF THERE'S A TILE COLOR FOR THIS ENTRY
                        tileColor = UIColor(red: CGFloat(tileColorArray[0]) / 255.0, green: CGFloat(tileColorArray[1]) / 255, blue: CGFloat(tileColorArray[2]) / 255, alpha: 1.0)
                    }
                    if let bgOneColorArray = levelDictionary["bgcolorlight"] as? Array<Int>{ // IF THERE'S A BG COLOR FOR THIS ENTRY
                        bgcolor_1 = UIColor(red: CGFloat(bgOneColorArray[0]) / 255.0, green: CGFloat(bgOneColorArray[1]) / 255, blue: CGFloat(bgOneColorArray[2]) / 255, alpha: 1.0)
                    }
                    if let bgTwoColorArray = levelDictionary["bgcolordark"] as? Array<Int>{ // IF THERE'S A BG COLOR FOR THIS ENTRY
                        bgcolor_2 = UIColor(red: CGFloat(bgTwoColorArray[0]) / 255.0, green: CGFloat(bgTwoColorArray[1]) / 255, blue: CGFloat(bgTwoColorArray[2]) / 255, alpha: 1.0)
                    }
                }else{ // IF THERE'S NO ENTRY FOR THIS LEVEL
                    tileColor = UIColor(red: 133.0 / 255.0, green: 125.0 / 255.0, blue: 18.0 / 255.0, alpha: 1.0) // DEFAULT TILE COLOR
                    bgcolor_1 = UIColor(red: 13.0 / 255.0, green: 125.0 / 255.0, blue: 46.0 / 255.0, alpha: 1.0) // DEFAULT BG 1
                    bgcolor_2 = UIColor(red: 22.0 / 255.0, green: 140.0 / 255.0, blue: 66.0 / 255.0, alpha: 1.0) // DEFAULT BG 2
                }
            }
        }
    }
    
    func resetSurvivalStats(){
        self.tilesinlevel = 1
        self.currentlevel = 0
        self.tilesinlevel = 1
        self.flipTime = 0.3
        self.gameTime = 0
        self.currentlevel = 0
        self.gamePoints = 0
        self.streakCount = 0
        self.luckiesCount = 0
        self.levelPoints = 0
    }
    
    func updatePoints(streakCount sCount: Int = 1, isItALucky lucky: Bool) -> Dictionary<String, Int>{
        var luckyPoints = 0
        if (lucky){
            luckyPoints = pointsForLucky
            luckiesCount++
        }
        let pointsToAdd = sCount * (pointsForMatch + luckyPoints)
        self.gamePoints += pointsToAdd
        self.levelPoints += pointsToAdd
        
        let pointsDictionary = ["pointsadded": pointsToAdd, "totalpoints": gamePoints]
        return pointsDictionary
    }
    
    func addEndBonus() -> Int{
        self.gamePoints += self.currentTime * pointsForSpeed
        self.gamePoints += self.errorsallowed * pointsForErrors
        self.levelPoints += self.currentTime * pointsForSpeed
        self.levelPoints += self.errorsallowed * pointsForErrors
        return (self.currentTime * pointsForSpeed) + (self.errorsallowed * pointsForErrors)
    }
}

extension CAGradientLayer {
    
    func turquoiseColor() -> CAGradientLayer {
        let topColor = UIColor.redColor()
        let bottomColor = UIColor.blackColor()
        
        let gradientColors: Array <AnyObject> = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: Array <NSNumber> = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        return gradientLayer
    }
}