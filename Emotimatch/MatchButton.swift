//
//  MatchButton.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/8/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class MatchButton: UIButton {
   
    var imageName: String = ""
    var matchId: Int = 0
    var matchIndex: Int = 0
    let matchButtonView = UIView()
    let matchImageView = UIView()
    var attempted: Bool = false
    let soundObject = EmojiAudio.sharedAudio
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    init(frame aFrame: CGRect, bgColor aColor: UIColor, initImage mName: String, initIndex mIndex: Int, matchId mId: Int) {
        
        super.init(frame: aFrame)
        
        //addTarget(self, action: Selector("buttonTouched:"), forControlEvents: .PrimaryActionTriggered)
        backgroundColor = UIColor.clearColor();
        self.matchId = mId
        
        self.matchIndex = mIndex
        self.imageName = mName
        self.tag = mIndex
        //self.setTitle(mName, forState: .Normal)

        matchButtonView.backgroundColor = aColor
        matchButtonView.makeCircle()
        matchButtonView.frame = CGRectMake(10, 10, self.frame.size.width - 20, self.frame.size.height - 20)
        matchImageView.frame = CGRectMake(10, 10, self.frame.size.width - 20, self.frame.size.height - 20)
        matchImageView.makeCircle()
        matchImageView.backgroundColor = UIColor.whiteColor()
        let matchImage = UIImageView()
            matchImage.frame = CGRectMake(self.matchImageView.frame.size.width * 0.2, self.matchImageView.frame.size.width * 0.2, self.matchImageView.frame.size.width * 0.6, self.matchImageView.frame.size.height * 0.6)
            matchImage.image = UIImage(named: mName)
            self.matchImageView.addSubview(matchImage)
        self.addSubview(matchButtonView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView != nil{
            soundObject.playMoveSound()
        }
    }
    
    /*override var highlighted: Bool {
        didSet {
            switch (highlighted) {
            case (true):
                print("clicked")
            case (false):
                print("unclicked")
            default:
                print("nothing")
            }
        }
    }*/

}

extension UIView {
    func makeCircle(){
        self.layer.cornerRadius = self.layer.frame.size.width / 2
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 3.0
    }
}
