//
//  SurvivalView.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/27/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit
import GameKit

class SurvivalView: UIViewController {

    let soundObject = EmojiAudio.sharedAudio
    
    private var matchArray:ArraySlice<Int>?
    private var buttonArray = [MatchButton]()
    private var allEmojiArray = Array(1...846)
    private var currentSelection = 0
    private var doubledArray: [NSArray] = []
    private var matchedArray: [Int] = []
    private var selectedArray: [Int] = []
    private var attemptsMade = 0
    private var matchesInGame:Int?
    private var matchesMatched = 0
    private let matchCount = 2
    private var tileIsFlipping = false
    private var streakCount = 0
    private var arrayOfAttempts:Array<Int> = []
    private var pointsDictionary: Dictionary<String, Int>?
    private var totalPoints = 0
    private var sInfo = SurvivalObject()
    private var sprite = SpriteView(frame: UIScreen.mainScreen().bounds )
    
    private var timer: NSTimer?
    private var currentPointsAdded: Int = 0
    private var currentPointsToAdd: Int?
    private var bonusTimerView = TimerView(frame: CGRectZero)
    
    @IBOutlet var backgroundGradient: GradientView?
    
    @IBOutlet var alertText: UILabel?
    @IBOutlet var errorsLeft: UILabel?
    @IBOutlet var levelLabel: UILabel?
    @IBOutlet var pointsAddedLabel: UILabel?
    @IBOutlet var totalPointsLabel: UILabel?
    
    // MARK: - Stats view outlets
    
    @IBOutlet var statsLevelPoints: UILabel?
    @IBOutlet var statsGamePoints: UILabel?
    @IBOutlet var statsStreak: UILabel?
    @IBOutlet var statsErrorBonus: UILabel?
    @IBOutlet var statsTimeBonus: UILabel?
    @IBOutlet var statsTotalMoves: UILabel?
    @IBOutlet var statsLuckies: UILabel?
    @IBOutlet var statsView: UIView?
    
    // MARK: - Lose view
    @IBOutlet var restartGameButton: UIButton?
    @IBOutlet var loseView: UIView?
    
    @IBOutlet var startNextLevel: UIButton?
    @IBOutlet var startGameButton: UIButton?
    @IBOutlet var startGameView: UIView?
    @IBOutlet var tileHolder: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorsLeft?.textColor = UIColor.whiteColor()
        tileHolder?.backgroundColor = UIColor.clearColor()
        totalPointsLabel?.textColor = UIColor.whiteColor()
        sprite.userInteractionEnabled = false
        self.view.addSubview(sprite)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Buttons
    
    @IBAction func startGame(sender: AnyObject) {
        setupGame()
        soundObject.playBackgroundMusic()
        startGameView?.hidden = true
        
        bonusTimerView.frame = CGRectMake(1700, 20, 70, 70)
        bonusTimerView.backgroundColor = UIColor.clearColor()
    }
    
    @IBAction func startNextLevel(sender: AnyObject) {
        startNextLevel?.removeFromSuperview()
        setupGame()
        statsView?.hidden = true
    }
    
    @IBAction func restartGame(sender: AnyObject) {
        updateScore()
        totalPoints = 0
        sInfo.resetSurvivalStats()
        loseView?.hidden = true
        soundObject.playBackgroundMusic()
        restartGameButton?.removeFromSuperview()
        setupGame()
    }
    
    @IBAction func goHome(sender: AnyObject) {
        totalPoints = 0
        updateScore()
        loseView?.hidden = true
        goBackHome()
    }
    
    @IBAction func buyExtraMove(sender: AnyObject) {
        sInfo.errorsallowed++
        let products = BuyPowerup()
            products.requestProductInfo()
        /*startTimer()
        loseView?.hidden = true
        for btn in buttonArray{
            btn.userInteractionEnabled = true;
            btn.preferredFocusedView
        }
        flipWrongTiles()
        errorsLeft?.text = "x \(sInfo.errorsallowed)"
        setNeedsFocusUpdate()*/
    }
    
    @IBAction func flipTiles(sender: AnyObject) {
        for btn in buttonArray{
            if !btn.matchImageView.isDescendantOfView(btn){
                UIView.transitionFromView(btn.matchButtonView, toView: btn.matchImageView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
            }
        }
        delay(0.3 + sInfo.flipTime){
            for btn in self.buttonArray{
                if !self.matchedArray.contains(btn.matchId) && !self.selectedArray.contains(btn.matchIndex) {
                    UIView.transitionFromView(btn.matchImageView, toView: btn.matchButtonView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
                }
            }
        }
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView != nil{
        context.nextFocusedView!.layer.shadowOffset = CGSizeMake(7, 7);
        context.nextFocusedView!.layer.shadowOpacity = 0.6;
        context.nextFocusedView!.layer.shadowRadius = 7;
        context.nextFocusedView!.layer.shadowColor = UIColor.blackColor().CGColor;
        }
        if (context.previouslyFocusedView != nil){
            context.previouslyFocusedView!.layer.shadowOpacity = 0;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup
    
    func setupGame(){
        sInfo.updateSurvivalStats()
        self.view.addSubview(bonusTimerView)
        bonusTimerView.progress = CGFloat(sInfo.gameTime)
        levelLabel?.text = "Level \(sInfo.currentlevel)"
        totalPointsLabel?.text = String(sInfo.gamePoints)
        
        clearVariables()
        var matchCounter = 1
        for (i, match) in matchArray!.enumerate(){ // DOUBLE THE ARRAY
            for _ in 1...matchCount{
                let matchWithIndex = [String(match), matchCounter, i + 1]
                matchCounter++
                doubledArray.append(matchWithIndex)
            }
        }
        var tileLayout: Dictionary<String, Int>?
        /*if matchArray!.count.isPrime() && matchArray!.count > 3{ // DIFFERENT LAYOUT NEEDS TO BE APPLIED
            
        }else{ // NORMAL LAYOUT
            tileLayout = CGFloat(doubledArray.count).getSquare
        }*/
            tileLayout = CGFloat(doubledArray.count).getLayout
        
        var tileCols = tileLayout!["columns"]
        let originalTileCols = tileCols
        let tileRows = tileLayout!["rows"]
        let extraTiles = tileLayout!["extra"]
        let tileSize = CGFloat(160)
        var tSize = tileSize
        if tileRows >= 6{
            tSize = tileSize * 0.75
        }
        if tileCols >= 14{
            tSize = tileSize * 0.6
        }
        if tileRows >= 8{
            tSize = tSize * 0.5
        }
        
        let tHolderInnerWidth = CGFloat(tileCols!) * tSize
        let shuffledArray = doubledArray.shuffle
        var tileYPos = CGFloat(0)
        var tileXCount = CGFloat(0)
        var rowCounter = 1;
        var moveAmt = CGFloat(0)
        if extraTiles > 0{
            moveAmt = tSize/(-2)
        }
        
        for (i , match) in shuffledArray.enumerate(){
            let matchBtn = MatchButton(frame: CGRectMake(tSize * tileXCount + 40 - moveAmt, tileYPos + 40, tSize, tSize), bgColor: (sInfo.tileColor)!, initImage: match[0] as! String, initIndex: match[1] as! Int, matchId: match[2] as! Int)
            //let matchBtn = MatchButton(frame: CGRectMake(tSize * tileXCount + (((tileHolder?.frame.size.width)! / 2) - (tHolderInnerWidth / 2)) - moveAmt, tileYPos + 40, tSize, tSize), bgColor: (sInfo.tileColor)!, initImage: match[0] as! String, initIndex: match[1] as! Int, matchId: match[2] as! Int)
            if Int(tileXCount)+1 == tileCols{
                rowCounter++
                if extraTiles > 0{
                    if rowCounter > ((tileRows! - extraTiles!) / 2) && rowCounter <= ((tileRows! - extraTiles!) / 2) + extraTiles! {
                        tileCols = originalTileCols! + 1
                        moveAmt = 0
                    }else{
                        tileCols = originalTileCols!
                        moveAmt = tSize/(-2)
                        tileCols = originalTileCols!
                    }
                }
                tileYPos += tSize
                tileXCount = 0
            }else{
                
                tileXCount++
            }
            matchBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            matchBtn.canBecomeFocused()
            matchBtn.userInteractionEnabled = false
            matchBtn.addTarget(self, action: Selector("buttonTouched:"), forControlEvents: .PrimaryActionTriggered)
            matchBtn.matchButtonView.makeCircle()
            self.tileHolder!.addSubview(matchBtn)
            buttonArray.append(matchBtn)
        }
        
        let tHolderHeight = (CGFloat(tileRows!) * tSize) + 80.0
        var tHolderWidth = (CGFloat(tileCols!) * tSize) + 80.0
        if extraTiles > 0{
            tHolderWidth = (CGFloat(tileCols! + 1) * tSize) + 80.0
        }
        tileHolder!.frame = CGRectMake((UIScreen.mainScreen().bounds.width/2) - tHolderWidth/2, 60 + (UIScreen.mainScreen().bounds.height/2) - tHolderHeight/2, tHolderWidth, tHolderHeight)
        tileHolder?.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)
        tileHolder?.layer.borderWidth = 4.0
        tileHolder?.layer.borderColor = UIColor.whiteColor().CGColor
        tileHolder?.layoutSubviews()
        backgroundGradient?.frame = (backgroundGradient?.frame)!
        backgroundGradient?.colors = [sInfo.bgcolor_1!, sInfo.bgcolor_2!]
        backgroundGradient?.locations = [0.0, 1.0]
        backgroundGradient?.direction = .Horizontal
        backgroundGradient?.mode = .Radial
        
        let extraErrors = sInfo.errorsallowed - sInfo.errorsForLevel!
        delay(1.0){
            for i in 0...extraErrors{
                self.delay(Double(i) * 0.2){
                    self.errorsLeft?.text = "x \(self.sInfo.errorsForLevel! + i)"
                }
            }
        }
        delay((Double(extraErrors) * 0.2) + 1.0){
            self.updateErrors() // UPDATE ERRORS LEFT IN GAME
        }
        
        delay(0.7){
            self.showAllTiles()
        }
    }
    
    func clearVariables(){
        attemptsMade = 0 // RESET ATTEMPTS COUNTER
        matchedArray = [] // CLEAR ARRAY OF CORRECT MATCHES
        matchArray = getLevelArray() // GET THE ARRAY OF TILES
        matchesMatched = 0 // CLEAR NUMBER OF MATCHES THAT HAVE BEEN MATCHED
        streakCount = 0 // CLEAR STREAK COUNT
        matchesInGame = matchArray?.count
        if buttonArray.count > 0{ // CLEAR BUTTON ARRAY AND BOARD
            for btn in buttonArray{
                self.view!.viewWithTag(btn.tag)?.removeFromSuperview()
            }
            buttonArray = []
            doubledArray = []
        }
    }
    
    func showAllTiles(){
        soundObject.playFlipSound()
        for btn in buttonArray{
            UIView.transitionFromView(btn.matchButtonView, toView: btn.matchImageView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
        }
        delay(sInfo.flipTime){
            self.hideAllTiles()
        }
    }
    
    func hideAllTiles() {
        soundObject.playFlipBackSound()
        startGameButton?.removeFromSuperview()
        for (i, btn) in buttonArray.enumerate(){
            UIView.transitionFromView(btn.matchImageView, toView: btn.matchButtonView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: { (fininshed: Bool) -> () in
                    btn.userInteractionEnabled = true;
                if i == 0{
                    self.tileHolder!.preferredFocusedView
                    self.tileHolder?.setNeedsFocusUpdate()
                    self.tileHolder?.updateFocusIfNeeded()
                }
            })
        }
        currentSelection = 0
        startTimer()
    }
    
    func startTimer(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateTimer:", userInfo: nil, repeats: true)
    }
    
    func updateTimer( timer : NSTimer ){
        sInfo.currentTime--
        self.bonusTimerView.progress = CGFloat(sInfo.currentTime)/CGFloat(sInfo.gameTime)
        if sInfo.currentTime == 0{
            stopTimer()
        }
        
    }
    
    func stopTimer(){
        timer?.invalidate()
    }
    
    func getLevelArray() -> ArraySlice<Int>{
        
        allEmojiArray.shuffle
        allEmojiArray = allEmojiArray.shuffle
        let levelArray = allEmojiArray.prefix(sInfo.tilesinlevel)
        return levelArray
    }
    
    func buttonTouched(sender: MatchButton) {
        if !sender.matchImageView.isDescendantOfView(sender) && !tileIsFlipping{
        UIView.transitionFromView(sender.matchButtonView, toView: sender.matchImageView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
        soundObject.playFlipSound()
        selectedArray.append(sender.matchIndex)
        if (currentSelection == 0){ // FIRST TILE WAS FLIPPED
            currentSelection = sender.matchId
        }else{ // SECOND TILE WAS FLIPPED
            if (sender.matchId == currentSelection){ // 2 SIMILAR TILES SELECTED (its a match)
                itsAMatch()
                matchedArray.append(currentSelection)
                var luckyMove = true // LUCKY STARTS OFF AS TRUE
                for btn in buttonArray{
                    if selectedArray.contains(btn.matchIndex){
                        if btn.attempted{ // IF ONE OF THE BUTTONS HAS BEEN ATTEMPTED, LUCKY IS NOW FALSE
                            luckyMove = false
                        }
                    }
                }
                if luckyMove{ // IF LUCKYMOVE IS STILL TRUE, IT'S A LUCKY MOVE
                    showAlert("LUCKYYYYY")
                    luckyFunction()
                }
                selectedArray = []
                currentSelection = 0
                matchesMatched++
                streakCount++
                if sInfo.streakCount < streakCount{ // IF THE CURRENT STREAK IS THE LONGEST, SET IT IN THE GAME OBJECT
                    sInfo.streakCount = streakCount // THIS WILL GIVE US THE LONGEST STREAK TO SHOW AT THE END OF EACH GAME
                }
                pointsDictionary = sInfo.updatePoints(streakCount: streakCount, isItALucky: luckyMove)
                pointsAddedLabel?.hidden = false
                pointsAddedLabel?.text = "YOU GOT \(pointsDictionary!["pointsadded"]!) POINTS!"
                
                rollUpPoints(pointsDictionary!["pointsadded"]!)
                
                delay(1.0){
                    self.pointsAddedLabel?.hidden = true
                }
            }else{ // WRONG TILES SELECTED
                tileIsFlipping = true
                streakCount = 0
                sInfo.errorsallowed--
                
                if sInfo.errorsallowed == 0 && matchesMatched < matchesInGame{
                    youLose()
                    tileIsFlipping = false
                }else{
                    delay(0.3){
                        self.soundObject.playWrongSound()
                    }
                    delay(0.7){
                        self.flipWrongTiles()
                    }
                }
            }
            updateErrors()
            attemptsMade++
            if matchesMatched == matchesInGame{
                stopTimer()
                delay(Double(sprite.itsAMatchFrames.count) * sprite.animationSpeed){
                   self.youWin()
                }
            }
            
        }
        }
    }
    
    func itsAMatch(){
        sprite.itsAMatch()
    }
    
    func luckyFunction(){
        soundObject.playLuckySound()
        for i in 1...30{
            let starX: CGFloat = CGFloat(randomInt(100, max: Int((backgroundGradient?.frame.size.width)! - 200)))
            let starY: CGFloat = CGFloat(randomInt(50, max: Int((backgroundGradient?.frame.size.height)! - 150)))
            let starS: CGFloat = CGFloat(randomInt(50, max: 130))
            let star = UIImageView(frame: CGRectMake(starX, starY, starS, starS))
            let starImage = UIImage(named: "98")
            star.image = starImage
            delay(Double(i) * 0.05){
                self.backgroundGradient?.addSubview(star)
                self.delay(0.1){
                    star.removeFromSuperview()
                }
            }
        }
    }
    func rollUpPoints(rollTo: Int){
        currentPointsAdded = 0
        currentPointsToAdd = rollTo
        let pointTimerSpeed = 0.5 / Double(currentPointsToAdd!)
        for index in 1...rollTo{
            delay(Double(index) * pointTimerSpeed){
                self.totalPoints++
                self.totalPointsLabel?.text = String(self.totalPoints)
            }
        }
    }
    
    func showAlert(alertMessage : String){
        alertText?.text = alertMessage
        alertText?.hidden = false
        delay(1.5){
            self.hideAlert()
        }
    }
    
    func updateErrors(){
        errorsLeft?.text = "x \(String(sInfo.errorsallowed))"
    }
    
    func hideAlert(){
        alertText?.hidden = true
    }
    
    func flipWrongTiles() {
        for btn in buttonArray{
            if selectedArray.contains(btn.matchIndex){
                
                btn.attempted = true
                UIView.transitionFromView(btn.matchImageView, toView: btn.matchButtonView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: { (fininshed: Bool) -> () in
                    btn.userInteractionEnabled = true;
                    self.tileIsFlipping = false
                })
                
            }
        }
        soundObject.playFlipBackSound()
        selectedArray = []
        currentSelection = 0
    }
    
    func youWin(){
        soundObject.playCheeringSound()
        let endBonus = sInfo.addEndBonus()
        rollUpPoints(endBonus)
        totalPointsLabel?.text = "x \(sInfo.gamePoints)"
        populateStatsView()
        clearGame()
        statsView?.addSubview(startNextLevel!)
        statsView?.hidden = false
        startNextLevel?.preferredFocusedView
        setNeedsFocusUpdate()
    }
    
    func populateStatsView(){
        statsLevelPoints?.text = String(sInfo.levelPoints)
        statsGamePoints?.text = String(sInfo.gamePoints)
        statsStreak?.text = String(streakCount)
        statsErrorBonus?.text = "\(sInfo.errorsallowed) X \(sInfo.pointsForErrors) = \(sInfo.errorsallowed * sInfo.pointsForErrors)"
        statsTimeBonus?.text = "\( sInfo.currentTime ) SECONDS"
        statsTotalMoves?.text = String(attemptsMade)
        statsLuckies?.text = String(sInfo.luckiesCount)
        if sInfo.gamePoints > storedStats.stats.integerForKey("hiscore"){
            storedStats.stats.setInteger(sInfo.gamePoints, forKey: "hiscore")
        }
    }
    
    func clearGame(){
        for btn in buttonArray{
            btn.userInteractionEnabled = false
        }
    }
    
    func youLose(){
        loseView?.addSubview(restartGameButton!)
        soundObject.stopBackgroundMusic()
        delay(0.2){
            self.soundObject.playLoseSound()
        }
        stopTimer()
        clearGame()
        loseView?.hidden = false
        restartGameButton?.preferredFocusedView
        setNeedsFocusUpdate()
    }
    
    func goBackHome(){
        soundObject.stopBackgroundMusic()
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func updateScore(){
        let leaderboardID = "emlb12345"
        let sScore = GKScore(leaderboardIdentifier: leaderboardID)
        sScore.value = Int64(totalPoints)
        
        //let localPlayer: GKLocalPlayer = GKLocalPlayer.localPlayer()
        
        GKScore.reportScores([sScore], withCompletionHandler: { (error: NSError?) -> Void in
            if error != nil {
                print(error?.localizedDescription)
            } else {
                print("Score submitted")
                
            }
        })
    }

    override func pressesBegan(presses: Set<UIPress>, withEvent event: UIPressesEvent?) {
        if(presses.first?.type == UIPressType.Menu) {
            sInfo.resetSurvivalStats()
            soundObject.stopBackgroundMusic()
            stopTimer()
        } else {
            // perform default action (in your case, exit)
            super.pressesBegan(presses, withEvent: event)
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}

func randomInt(min: Int, max:Int) -> Int {
    return min + Int(arc4random_uniform(UInt32(max - min + 1)))
}

extension CGFloat {
    var getLayout:Dictionary<String, Int>{
        var n = self
        while Int(floor(n / 2)).isPrime() && n/2 > 2 && self != 6 || Int(n).isPrime(){
            n--
        }
        var rows = floor(sqrt(n))
        while floor(n/rows) != n/rows{
            rows--
        }
        let columns = n/rows
        let extras = self - (rows * columns)
        var newRows = rows
        var newCols = columns
        if extras > rows - 2{
            newRows = columns
            newCols = rows
        }
        return ["columns": Int(newCols), "rows": Int(newRows), "extra" : Int(extras)]
    }
}

extension Int {
    func divides(a: Int, b: Int) -> Bool {
        return a % b == 0
    }
    
    func countDivisors(number: Int) -> Int {
        var cnt = 0
        for i in 1...number {
            if divides(number, b: i) {
                ++cnt
            }
        }
        return cnt
    }
    
    func isPrime() -> Bool {
        let number = self
        return countDivisors(number) == 2
    }
}

extension Float {
    func roundToInt() -> Int{
        let value = Int(self)
        let f = self - Float(value)
        if f < 0.5{
            return value
        } else {
            return value + 1
        }
    }
}