//
//  SpriteView.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 11/12/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit
import SpriteKit

class SpriteView: UIView {

    var itsAMatchFrames = [SKTexture]()
    var matchSprite : SKSpriteNode?
    let scene = SKScene()
    var animationSpeed = 0.04
    var spriteViewer: SKView?
    
    override init(frame: CGRect) {
        print("initing")
        super.init(frame: frame)
        scene.size = self.bounds.size
        spriteViewer = SKView(frame: CGRectMake(0, 0, bounds.width, bounds.height))
        spriteViewer!.showsFPS = false
        spriteViewer!.showsNodeCount = false
        spriteViewer!.ignoresSiblingOrder = true
        scene.backgroundColor = UIColor.clearColor()
        spriteViewer!.backgroundColor = UIColor.clearColor()
        scene.scaleMode = .ResizeFill
        spriteViewer!.presentScene(scene)
        spriteViewer!.userInteractionEnabled = false
        self.addSubview(spriteViewer!)
        
        setupMatchSprite()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupMatchSprite(){
        let matchAnimatedAtlas = SKTextureAtlas(named: "ItsAMatch3")
        
        let numImages = matchAnimatedAtlas.textureNames.count
        for var i=0; i<=numImages-1; i++ {
            let matchTextureName = "ItsAMatch\(i)"
            itsAMatchFrames.append(matchAnimatedAtlas.textureNamed(matchTextureName))
        }
        
        let firstFrame = itsAMatchFrames[0]
        matchSprite = SKSpriteNode(texture: firstFrame)
        matchSprite!.position = CGPoint(x:CGRectGetMidX(spriteViewer!.frame), y:CGRectGetMidY(spriteViewer!.frame) - 60)
        SKTextureAtlas.preloadTextureAtlases([matchAnimatedAtlas], withCompletionHandler: {() -> Void in
            print("loaded")
        })
    }
    
    func itsAMatch(){
        if scene.children.count > 0{
            scene.removeAllChildren()
        }
        scene.addChild(matchSprite!)
        matchAnimation(itsAMatchFrames)
    }
    
    func matchAnimation(texture: [SKTexture]) {
        print(itsAMatchFrames.count)
        matchSprite!.runAction(SKAction.repeatAction(SKAction.animateWithTextures(itsAMatchFrames,
            timePerFrame: animationSpeed,
            resize: false,
            restore: false), count: 1))
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            matchSprite?.runAction(SKAction.playSoundFileNamed("match.wav", waitForCompletion: false))
        }
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
