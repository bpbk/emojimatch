//
//  GameObject.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/22/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

class GameObject: NSObject {
    var gameName: String
    var tileArray: Array<String>
    var movesInGame: Int
    
    init(nameOfGame gName: String, tileArray tiles: Array<String>, gameMoves gMoves: Int) {
        self.gameName = gName
        self.tileArray = tiles
        self.movesInGame = gMoves
        super.init()
    }
}