//
//  EmojiAudio.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 11/11/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

class EmojiAudio: NSObject {
    static let sharedAudio = EmojiAudio()
    var flipSound: SystemSoundID?
    var flipBackSound: SystemSoundID?
    var luckySound: SystemSoundID?
    var wrongSound: SystemSoundID?
    var moveSound: SystemSoundID?
    var matchSound: SystemSoundID?
    var cheeringSound: SystemSoundID?
    var loseSound: SystemSoundID?
    var backgroundMusic: AVAudioPlayer = {
        let url:NSURL = NSBundle.mainBundle().URLForResource("Banking_Future_1", withExtension: "mp3")!
        let player = try! AVAudioPlayer(contentsOfURL: url, fileTypeHint: "error")
        player.numberOfLoops = -1
        return player
    }()
    
    override init() {
        super.init()
        flipSound = createFlipSound()
        flipBackSound = createFlipBackSound()
        luckySound = createLuckySound()
        wrongSound = createWrongSound()
        moveSound = createMovementSound()
        cheeringSound = createCheeringSound()
        matchSound = createMatchSound()
        loseSound = createLoseSound()
    }
    
    func playBackgroundMusic(){
        if audioStatus.audioToggle.boolForKey("musicToggle"){
            backgroundMusic.play()
        }
    }
    
    func stopBackgroundMusic(){
        if audioStatus.audioToggle.boolForKey("musicToggle"){
            backgroundMusic.stop()
            backgroundMusic.currentTime = 0.0
        }
    }
    
    func playMoveSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(moveSound!)
        }
    }
    func playFlipSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(flipSound!)
        }
    }
    func playFlipBackSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(flipBackSound!)
        }
    }
    func playLuckySound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(luckySound!)
        }
    }
    func playWrongSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(wrongSound!)
        }
    }
    func playCheeringSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(cheeringSound!)
        }
    }
    func playMatchSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(matchSound!)
        }
    }
    func playLoseSound(){
        if audioStatus.audioToggle.boolForKey("fxToggle"){
            AudioServicesPlaySystemSound(loseSound!)
        }
    }
}


func createMovementSound() -> SystemSoundID {
    var soundID: SystemSoundID = 0
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "move2", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createFlipSound() -> SystemSoundID {
    var soundID: SystemSoundID = 1
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "flip", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createFlipBackSound() -> SystemSoundID {
    var soundID: SystemSoundID = 2
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "flipback", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createLuckySound() -> SystemSoundID {
    var soundID: SystemSoundID = 3
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "lucky", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createWrongSound() -> SystemSoundID {
    var soundID: SystemSoundID = 4
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "wrong", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createCheeringSound() -> SystemSoundID {
    var soundID: SystemSoundID = 5
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "cheering", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createMatchSound() -> SystemSoundID {
    var soundID: SystemSoundID = 6
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "match", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
func createLoseSound() -> SystemSoundID {
    var soundID: SystemSoundID = 7
    let soundURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), "lose", "wav", nil)
    AudioServicesCreateSystemSoundID(soundURL, &soundID)
    return soundID
}
