//
//  MainView.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/6/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit
import GameKit

class MainView: UIViewController, GKGameCenterControllerDelegate {
    
    private let reuseIdentifier = "matchCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private let gamesArray = [
        ["tilecount": 9, "name": "animals"],
    ]
    var gcEnabled = Bool() // GAME CENTER ENABLED?
    var gcDefaultLeaderBoard = String() // LEADERBOARD ID
    
    @IBOutlet var hiscore: UILabel?
    
    @IBOutlet var introView: GradientView?
    @IBOutlet var titleLogo: UIImageView?
    
    @IBOutlet var mapViewButton: UIButton?
    @IBOutlet var surviveViewButton: UIButton?
    
    @IBOutlet var FXToggleButton: UIButton?
    @IBAction func FXToggle(sender: AnyObject) {
        print(audioStatus.audioToggle.boolForKey("fxToggle"))
        if audioStatus.audioToggle.boolForKey("fxToggle"){ // FX ARE ON. TURN THEM OFF
            audioStatus.audioToggle.setBool(false, forKey: "fxToggle")
            FXToggleButton?.setTitle("TURN FX ON", forState: UIControlState.Normal)
        }else{ // FX ARE OFF. TURN THEM ON
            audioStatus.audioToggle.setBool(true, forKey: "fxToggle")
            FXToggleButton?.setTitle("TURN FX OFF", forState: UIControlState.Normal)
        }
    }
    @IBOutlet var musicToggleButton: UIButton?
    @IBAction func musicToggle(sender: AnyObject) {
        if audioStatus.audioToggle.boolForKey("musicToggle"){ // MUSIC IS ON. TURN IT OFF
            audioStatus.audioToggle.setBool(false, forKey: "musicToggle")
            musicToggleButton?.setTitle("TURN MUSIC ON", forState: UIControlState.Normal)
        }else{ // MUSIC IS OFF. TURN IT ON
            audioStatus.audioToggle.setBool(true, forKey: "musicToggle")
            musicToggleButton?.setTitle("TURN MUSIC OFF", forState: UIControlState.Normal)
        }
    }
    
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func authenticateLocalPlayer() {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(MainView, error) -> Void in
            if((MainView) != nil) {
                // 1 Show login if player is not logged in
                self.presentViewController(MainView!, animated: true, completion: nil)
                print("not logged in")
            } else if (localPlayer.authenticated) {
                // 2 Player is already euthenticated & logged in, load game center
                self.gcEnabled = true
                
                // Get the default leaderboard ID
                localPlayer.loadDefaultLeaderboardIdentifierWithCompletionHandler({ (leaderboardIdentifer: String?, error: NSError?) -> Void in
                    if error != nil {
                        print(error)
                    } else {
                        self.gcDefaultLeaderBoard = leaderboardIdentifer!
                    }
                })
                
                
            } else {
                // 3 Game center is not enabled on the users device
                self.gcEnabled = false
                print("Local player could not be authenticated, disabling game center")
                print(error)
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.authenticateLocalPlayer()
        introView!.frame = (self.view.frame)
        introView!.colors = [UIColor(red: 91.0 / 255.0, green: 241.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0), UIColor(red: 0.0 / 255.0, green: 202.0 / 255.0, blue: 218.0 / 255.0, alpha: 1.0)]
        introView!.locations = [0.0, 1.0]
        introView!.direction = .Horizontal
        introView!.mode = .Radial
        print(UIScreen.mainScreen().bounds.size.width)
        openingSequence()
        
        //EmojiAudio()
        //let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //matchCollection = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        //matchCollection!.dataSource = self
        //matchCollection!.delegate = self
        //matchCollection!.registerClass(MatchCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        if audioStatus.audioToggle.boolForKey("musicToggle") == false{
            musicToggleButton?.setTitle("TURN MUSIC ON", forState: UIControlState.Normal)
        }
        if audioStatus.audioToggle.boolForKey("fxToggle") == false{
            FXToggleButton?.setTitle("TURN FX ON", forState: UIControlState.Normal)
        }
        
        self.hiscore?.text = String(storedStats.stats.integerForKey("hiscore"))
    }
    
    func openingSequence(){
        
        delay(0.5){
            UIView.animateWithDuration(0.5, animations: {
                self.titleLogo!.frame.origin.y -= 200
            })
        }
        delay(1.0){
            UIView.animateWithDuration(0.5, animations: {
                    self.mapViewButton?.alpha = 1.0
                    self.surviveViewButton?.alpha = 1.0
                }, completion:{_ in
                    self.introView!.preferredFocusedView
                    self.introView?.setNeedsFocusUpdate()
                    self.introView?.updateFocusIfNeeded()
            })
        }
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView != nil{
        context.nextFocusedView!.layer.shadowOffset = CGSizeMake(0, 10);
        context.nextFocusedView!.layer.shadowOpacity = 0.6;
        context.nextFocusedView!.layer.shadowRadius = 15;
        context.nextFocusedView!.layer.shadowColor = UIColor.blackColor().CGColor;
        if (context.previouslyFocusedView != nil){
            context.previouslyFocusedView!.layer.shadowOpacity = 0;
        }
        }
    }
    
    // HERE IS SOME COLLECTION VIEW STUFF IN CASE WE NEED IT LATER
    /*func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gamesArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MatchCollectionViewCell
        cell.matchView?.backgroundColor = UIColor.whiteColor()
        let gameName = gamesArray[indexPath.row]
        cell.gameName.text = gameName["name"] as? String
        // Configure the cell
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        currentSelection = indexPath.item
        //let currentView = collectionView.cellForItemAtIndexPath(indexPath) as! MatchCollectionViewCell
        
        collectionView.reloadData()
        //collectionView.reloadItemsAtIndexPaths([indexPath])
        
        print(indexPath)
    }*/
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "startgame") {
            //let game = segue.destinationViewController as! GameView
            //game.gameInfo = GameObject(nameOfGame: "name of game", tileArray: ["278", "405", "295", "243", "284"], gameMoves: 12)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }

}

// COLLECTION VIEW FLOW LAYOUT STUFF IF WE NEED IT LATER
/*extension MainView : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            //let matchCell = self.collectionView?.cellForItemAtIndexPath(indexPath)
            //2
            /*if var size = matchCell?.frame.size {
            size.width += 10
            size.height += 10
            return size
            }*/
            return CGSize(width: 200, height: 200)
    }
    
    //3
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
}*/

extension Array {
    var shuffle:[Element] {
        var elements = self
        for index in 0..<elements.count {
            let newIndex = Int(arc4random_uniform(UInt32(elements.count-index)))+index
            if index != newIndex { // Check if you are not trying to swap an element with itself
                swap(&elements[index], &elements[newIndex])
            }
        }
        return elements
    }
    func groupOf(n:Int)-> [[Element]] {
        var result:[[Element]]=[]
        for i in 0...(count/n)-1 {
            var tempArray:[Element] = []
            for index in 0...n-1 {
                tempArray.append(self[index+(i*n)])
            }
            result.append(tempArray)
        }
        
        return result
    }
}

struct storedStats {
    static var stats = NSUserDefaults.standardUserDefaults()
}

struct audioStatus {
    static var audioToggle = NSUserDefaults.standardUserDefaults()
}

struct gamestats {
    static var currentlives = 0
}
