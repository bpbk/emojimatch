//
//  MatchCollectionView.swift
//  Emotimatch
//
//  Created by Brandon Phillips on 10/2/15.
//  Copyright © 2015 Operation:CMYK. All rights reserved.
//

import UIKit

private let reuseIdentifier = "matchCell"
private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
var currentSelection = Int()

class MatchCollectionView: UICollectionViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.canBecomeFocused()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    override func collectionView(collectionView: UICollectionView, shouldUpdateFocusInContext context: UICollectionViewFocusUpdateContext) -> Bool {
        guard let indexPaths = collectionView.indexPathsForSelectedItems() else { return true }
        
        return indexPaths.isEmpty
    }

    /*override func collectionView(collectionView: UICollectionView, didUpdateFocusInContext context: UICollectionViewFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if collectionView == self.collectionView {
            self.collectionView?.collectionViewLayout.invalidateLayout()
            let cell = self.collectionView?.cellForItemAtIndexPath(context.nextFocusedIndexPath!) as! MatchCollectionViewCell
            if (context.previouslyFocusedIndexPath != nil){
                let lastcell = self.collectionView?.cellForItemAtIndexPath(context.previouslyFocusedIndexPath!) as! MatchCollectionViewCell
                lastcell.matchView.layer.borderWidth = 0
                lastcell.matchView.layer.borderColor = UIColor.blackColor().CGColor
            }
            cell.matchView.layer.borderWidth = 2
            cell.matchView.layer.borderColor = UIColor.blueColor().CGColor
            

            /*UIView.animateWithDuration(1, animations:
                {
                    cell?.frame = CGRectMake(cell!.frame.origin.x - 40 , cell!.frame.origin.y - 40, cell!.frame.size.width + 20, cell!.frame.size.height)
                })*/
            
            // Animate
            print(context.nextFocusedIndexPath)
        }
    }*/
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 10
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MatchCollectionViewCell
        // Configure the cell
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
            currentSelection = indexPath.item
            collectionView.reloadData()
        
            print(indexPath)
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        context.nextFocusedView!.layer.shadowOffset = CGSizeMake(0, 10);
        context.nextFocusedView!.layer.shadowOpacity = 0.6;
        context.nextFocusedView!.layer.shadowRadius = 15;
        context.nextFocusedView!.layer.shadowColor = UIColor.blackColor().CGColor;
        if (context.previouslyFocusedView != nil){
            context.previouslyFocusedView!.layer.shadowOpacity = 0;
        }
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}

extension MatchCollectionView : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            //let matchCell = self.collectionView?.cellForItemAtIndexPath(indexPath)
            //2
            /*if var size = matchCell?.frame.size {
                size.width += 10
                size.height += 10
                return size
            }*/
            return CGSize(width: 350, height: 200)
    }
    
    //3
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
}
